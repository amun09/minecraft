# Minecraft für Offene Jugendarbeit in Spandau
## Beschreibung
Ein kurze Anleitung für die Einrichtung zum Beitritt
auf den Minecraft Citybuildserver des Medienkompetenzzentrums CIA Spandau.

# Installation
- Verraussetzung: Java 8 ist installiert!
- Lade die beiden ZIP-Archive aus den Installationsverzeichnissen herrunter
- Entpacke UltimMC.zip
- Speicher den neunen Ordner UltimMC an einem Ort deiner Wahl
- Starte UltimMC
    - Wähle deine Sprache aus
    - Wähle deine java 8 Version aus
    - Gibt dem Modpack mindestens 4096 MiB Arbeitspeicher aus
    - Wähle deine Option zum Senden von Nutzerdaten (empfohlen wird den Hacken zu entfernen)
- Instanz hinzufügen
    - oben links
    - Importiere ZIP-Datei
    - Wähle das ZIP-Archiv Offizielles CIA-Modpack-X.X.X-VX.zip aus
    - Das Modpack wird jetzt eingerichtet
- Profil erstellen
    - oben rechts 
    - Konten verwalten 
    - Add Account
    - Wähle: Benutzername und DeinPasswort 
    - Wähle: Hacken bei local
- Jetzt sind wir bereit zu Spielen!
# Verwendung
- UltimMC starten
- Starte Instanz Offizielles CIA-Modpack-X.X.X-VX.zip
- Multiplayer
- Wähle den Offiziellen CIA-Server aus
    - Erstes Betreten
        ``` bash
        /register DeinPasswort DeinPasswort
        ```
    - Ab dem 2. Betreten
        ``` bash
        /login DeinPasswort 
        ```
# Support
- Bei allgemeinen Fragen stellt diese bitte auf unserem [Discord](https://discord.gg/AEH2qGr)
- Bei Fragen und Problemen zu und mit den verwendedten Mods wird auf das Supportangebot des Entwickler verwiesen.
# Authors and acknowledgment
Die Mods wurden von ihren jeweiligen Urhebern erstellt und es handelt sich hier nur um eine, für die Arbeit im Medienkompetenzzentrums, zusammengestellte Sammlung.
# Projektstatus
In unregelmäßigen Abständen und unabhängig von der Entwicklung der einzelnen Mods wird das hier zur Verfügung gestelltes Modpack und die zip des Lauchners geupdated.
